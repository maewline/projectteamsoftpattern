/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.projectteam.order;

import com.mycompany.projectteam.model.Product;
import java.awt.GridLayout;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class ProductListPanel extends javax.swing.JPanel implements OnBuyProductListener{

    private final ArrayList<Product> productList;
    private OrderPanel orderPanel;
    private ArrayList<OnBuyProductListener> subscriberLsit = new ArrayList<>();
    /**

    /**
     * Creates new form ProductListPanel
     */
    public ProductListPanel() {
        initComponents();

        productList = Product.getMockProductList();

        for (Product product : productList) {
            ProductPanel panel = new ProductPanel(product);
            productPanel.add(panel);
            panel.addOnBuyListener(this);

        }
        int num = productPanel.getComponentCount();
        int col = 3;
        productPanel.setLayout(new GridLayout(num / col + (num % col > 0 ? 1 : 0), col));
    }
    
    public void addOnButListener(OnBuyProductListener subscriber){
        subscriberLsit.add(subscriber);
    }
    public void setorderPanel(OrderPanel orderPanel){
        this.orderPanel = orderPanel;
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        productPanel = new javax.swing.JPanel();

        productPanel.setBackground(new java.awt.Color(239, 205, 186));

        javax.swing.GroupLayout productPanelLayout = new javax.swing.GroupLayout(productPanel);
        productPanel.setLayout(productPanelLayout);
        productPanelLayout.setHorizontalGroup(
            productPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 742, Short.MAX_VALUE)
        );
        productPanelLayout.setVerticalGroup(
            productPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 426, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(productPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 763, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel productPanel;
    // End of variables declaration//GEN-END:variables
 public void buy(Product product, int amount) {
        for (OnBuyProductListener s : subscriberLsit) {
            s.buy(product, amount);
        }
    }


}
