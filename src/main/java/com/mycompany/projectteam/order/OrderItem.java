/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.order;

import com.mycompany.projectteam.model.Product;

/**
 *
 * @author acer
 */
public class OrderItem {

    Product product;
    int amount;

    public OrderItem(Product product, int amount) {
        this.product = product;
        this.amount = amount;
    }
        public OrderItem() {

    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
}
