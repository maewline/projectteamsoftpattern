/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.dao;

import com.mycompany.projectteam.database.helper.DatabaseHelper;
import com.mycompany.projectteam.model.CheckListItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hanam
 */
public class CheckListItemDao implements Dao<CheckListItem>{
    @Override
    public CheckListItem get(int id) {
        CheckListItem item = null;
        String sql = "SELECT * FROM Check_List_Item WHERE CLI_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = CheckListItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<CheckListItem> getAll() {
        ArrayList<CheckListItem> list = new ArrayList();
        String sql = "SELECT * FROM Check_List_Item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckListItem item = CheckListItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<CheckListItem> getAll(String where, String order) {
        ArrayList<CheckListItem> list = new ArrayList();
        String sql = "SELECT * FROM Check_List_Item where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckListItem item = CheckListItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    


    @Override
    public CheckListItem save(CheckListItem checkListItem) {
        String sql = "INSERT INTO Check_List_Item (CLI_Quantity,MT_Unit,MT_Name,MT_Min,MT_QOH,CL_ID,MT_ID)"
                + "VALUES( ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, checkListItem.getQuantity());
            stmt.setString(2, checkListItem.getUnit());
            stmt.setString(3, checkListItem.getName());
            stmt.setInt(4, checkListItem.getMin());
            stmt.setInt(5, checkListItem.getQoh());
            stmt.setInt(6, checkListItem.getChecklist().getId());
            stmt.setInt(7, checkListItem.getMaterial().getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            checkListItem.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return checkListItem;
    }

    @Override
    public CheckListItem update(CheckListItem checkListItem) {
        String sql = "UPDATE Check_List_Item"
                + " SET CLI_Quantity = ?, MT_Unit = ?, MT_Name = ?, MT_Min = ?, MT_QOH = ? , CL_ID = ?, MT_ID = ?"
                + " WHERE CLI_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setInt(1, checkListItem.getQuantity());
            stmt.setString(2, checkListItem.getUnit());
            stmt.setString(3, checkListItem.getName());
            stmt.setInt(4, checkListItem.getMin());
            stmt.setInt(5, checkListItem.getQoh());
            stmt.setInt(6, checkListItem.getChecklist().getId());
            stmt.setInt(7, checkListItem.getMaterial().getId());
            stmt.setInt(8, checkListItem.getId());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckListItem checkListItem) {
        String sql = "DELETE FROM Check_List_Item WHERE CLI_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, checkListItem.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;      
    }
}

