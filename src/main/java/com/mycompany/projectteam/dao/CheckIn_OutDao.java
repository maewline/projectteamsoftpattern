/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.dao;

import com.mycompany.projectteam.database.helper.DatabaseHelper;
import com.mycompany.projectteam.model.CheckIn_Out;
import com.mycompany.projectteam.model.Employee;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author User
 */
public class CheckIn_OutDao implements Dao<CheckIn_Out> {

    @Override
    public CheckIn_Out get(int id) {
        CheckIn_Out item = null;
        String sql = "SELECT * FROM CheckIn_Out WHERE CIO_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = CheckIn_Out.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<CheckIn_Out> getAll() {
        ArrayList<CheckIn_Out> list = new ArrayList();
        String sql = "SELECT * FROM CheckIn_Out";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckIn_Out item = CheckIn_Out.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckIn_Out> getAll(String where, String order) {
        ArrayList<CheckIn_Out> list = new ArrayList();
        String sql = "SELECT * FROM CheckIn_Out where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckIn_Out item = CheckIn_Out.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckIn_Out> getAll(String order) {
        ArrayList<CheckIn_Out> list = new ArrayList();
        String sql = "SELECT * FROM CheckIn_Out ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckIn_Out item = CheckIn_Out.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
     public List<CheckIn_Out> getAllUnPaid() {
        ArrayList<CheckIn_Out> list = new ArrayList();
        String sql = "SELECT * FROM CheckIn_Out WHERE CIO_Payment_Status =\"N\" ORDER BY CIO_Time_Start asc; ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckIn_Out item = CheckIn_Out.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     
     public List<CheckIn_Out> getUnPaid(int EP_ID) {
        ArrayList<CheckIn_Out> list = new ArrayList();
        String sql = "SELECT * FROM CheckIn_Out WHERE EP_ID = "+EP_ID+" AND CIO_Payment_Status =\"N\" ORDER BY CIO_Time_Start asc;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckIn_Out item = CheckIn_Out.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckIn_Out save(CheckIn_Out obj) {
        String sql = "INSERT INTO CheckIn_Out (CIO_Time_Start, CIO_Time_End, CIO_Hour ,CIO_Payment_Status, EP_ID, S_ID)"
                + "VALUES (?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm",Locale.UK);
            stmt.setString(1, sdf.format(obj.getTime_start()));
            stmt.setString(2, sdf.format(obj.getTime_end()));
            stmt.setInt(3, obj.getHour());
            stmt.setString(4, obj.getPayment_status());
            stmt.setInt(5, obj.getEmployee().getId());
              stmt.setInt(6, obj.getSalary());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckIn_Out update(CheckIn_Out obj) {
        String sql = "UPDATE CheckIn_Out"
                + " SET CIO_Time_Start = ? ,CIO_Time_End = ? ,CIO_Hour = ?,CIO_Payment_Status = ?,EP_ID = ? ,S_ID = ?"
                + " WHERE CIO_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(7, obj.getId());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, sdf.format(obj.getTime_start()));
            stmt.setString(2, sdf.format(obj.getTime_end()));
            stmt.setInt(3, obj.getHour());
            stmt.setString(4, obj.getPayment_status());
             stmt.setInt(5, obj.getEmployee().getId());
             stmt.setInt(6, obj.getSalary());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckIn_Out obj) {
        String sql = "DELETE FROM CheckIn_Out WHERE CIO_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<CheckIn_Out> getDate(int id) {
        ArrayList<CheckIn_Out> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\",CIO_Time_Start) as period\n"
                + "FROM CheckIn_Out\n"
                + "WHERE " + id + "\"\n";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckIn_Out item = CheckIn_Out.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
