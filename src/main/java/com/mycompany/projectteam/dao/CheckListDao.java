/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.dao;

import com.mycompany.projectteam.database.helper.DatabaseHelper;
import com.mycompany.projectteam.model.CheckList;
import com.mycompany.projectteam.model.CheckListItem;
import com.mycompany.projectteam.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hanam
 */
public class CheckListDao implements Dao<CheckList>  {
    @Override
    public CheckList get(int id) {
        CheckList item = null;
        String sql = "SELECT * FROM Check_List WHERE CL_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = CheckList.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<CheckList> getAll() {
        ArrayList<CheckList> list = new ArrayList();
        String sql = "SELECT * FROM Check_List";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckList item = CheckList.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<CheckList> getAll(String where, String order) {
        ArrayList<CheckList> list = new ArrayList();
        String sql = "SELECT * FROM Check_List where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckList item = CheckList.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    


    @Override
    public CheckList save(CheckList checkList) {
        String sql = "INSERT INTO Check_List (CL_Date,EP_ID)"
                + "VALUES( ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            stmt.setString(1, sdf.format(checkList.getDate()));
            stmt.setInt(2, checkList.getEmployee().getId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            checkList.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return checkList;
    }

    @Override
    public CheckList update(CheckList checkList) {
        String sql = "UPDATE Check_List"
                + " SET CL_Date = ?, EP_ID = ?"
                + " WHERE CL_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(3, checkList.getId());
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            stmt.setString(1, sdf.format(checkList.getDate()));
            stmt.setInt(2, checkList.getEmployee().getId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
//            checkList.setId(id);
            //Add Item
//            checkList.setId(id);
//            for(CheckListItem od:checkList.getOrderDetails()){
//                CheckListItem detail = orderDetailDao.save(od);
//                if(detail == null){
//                    DatabaseHelper.endTransactionWithRollback();
//                    return null;
//                }
//            }
//            DatabaseHelper.endTransactionWithCommit();
            
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckList checkList) {
        String sql = "DELETE FROM Check_List WHERE CL_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, checkList.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
