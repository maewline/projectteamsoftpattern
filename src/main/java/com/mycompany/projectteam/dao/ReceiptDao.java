/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.dao;

import com.mycompany.projectteam.database.helper.DatabaseHelper;
import com.mycompany.projectteam.model.Member;
import com.mycompany.projectteam.model.Receipt;
import com.mycompany.projectteam.model.Receipt_Item;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author acer
 */
public class ReceiptDao  implements Dao<Receipt>{

    @Override
    public Receipt get(int id) {
        Receipt receipt = null;
        String sql = "SELECT * FROM Receipt WHERE RC_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;
    }

    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {
        Receipt_ItemDao rcItem = new Receipt_ItemDao();
        MemberDao memberDao = new MemberDao();
        Member member = memberDao.get(obj.getM_id());
        member.setScore(member.getScore() - obj.getDiscount());
        memberDao.update(member);
        String sql = "INSERT INTO receipt (RC_Timestamp,RC_Total,RC_Discount,RC_Cash,EP_ID,M_ID,RC_Qty)"
                + "VALUES (?, ?, ?, ?, ?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
            stmt.setString(1, df.format(obj.getTimestamp()));
            stmt.setDouble(2, obj.getTotal());
            stmt.setInt(3, obj.getDiscount());
            stmt.setDouble(4, obj.getCash());
            stmt.setInt(5, obj.getEp_id());
            stmt.setInt(6, obj.getM_id());
            stmt.setInt(7, obj.getQty());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            for (Receipt_Item receipt_Item : obj.getReceiptItem()) {
                Receipt_Item rc = rcItem.save(receipt_Item, id);

            }
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }
        public Receipt save(Receipt obj,int score) {
        Receipt_ItemDao rcItem = new Receipt_ItemDao();
        MemberDao memberDao = new MemberDao();
        Member member = memberDao.get(obj.getM_id());
        member.setScore(score);
        memberDao.update(member);
        String sql = "INSERT INTO receipt (RC_Timestamp,RC_Total,RC_Discount,RC_Cash,EP_ID,M_ID,RC_Qty)"
                + "VALUES (?, ?, ?, ?, ?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
            stmt.setString(1, df.format(obj.getTimestamp()));
            stmt.setDouble(2, obj.getTotal());
            stmt.setInt(3, obj.getDiscount());
            stmt.setDouble(4, obj.getCash());
            stmt.setInt(5, obj.getEp_id());
            stmt.setInt(6, obj.getM_id());
            stmt.setInt(7, obj.getQty());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            for (Receipt_Item receipt_Item : obj.getReceiptItem()) {
                Receipt_Item rc = rcItem.save(receipt_Item, id);

            }
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public Receipt update(Receipt obj) {
//        String sql = "UPDATE Receipt"
//                + " SET RC_Timestamp = ?, RC_Total = ? , RC_Discount = ? ,RC_Cash = ?, EP_ID = ?, M_ID = ?"
//                + " WHERE RC_ID = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getTimestamp());
//            stmt.setDouble(2, obj.getTotal());
//            stmt.setInt(3, obj.getDiscount());
//            stmt.setDouble(4, obj.getCash());
//            stmt.setInt(5, obj.getEp_id());
//            stmt.setInt(6, obj.getM_id());
//            stmt.setInt(7, obj.getId());
//            int ret = stmt.executeUpdate();
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
        return null;
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM receipt WHERE RC_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
