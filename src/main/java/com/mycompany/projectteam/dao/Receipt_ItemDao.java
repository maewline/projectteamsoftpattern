/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.dao;

import com.mycompany.projectteam.database.helper.DatabaseHelper;
import com.mycompany.projectteam.model.Receipt_Item;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class Receipt_ItemDao implements Dao<Receipt_Item> {
@Override
    public Receipt_Item get(int id) {
        Receipt_Item item = null;
        String sql = "SELECT * FROM receipt_item WHERE RCIT_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Receipt_Item.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Receipt_Item> getAll() {
        ArrayList<Receipt_Item> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt_Item item = Receipt_Item.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt_Item save(Receipt_Item obj) {
        String sql = "INSERT INTO receipt_item (PD_Name,RCIT_Amount,RCIT_Price,RCIT_Total,RC_ID,PD_ID)"
                + "VALUES (?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, obj.getPd_name());
            stmt.setInt(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());
            stmt.setDouble(4, obj.getTotal());
            stmt.setInt(5, obj.getRc_id());
            stmt.setInt(6, obj.getPd_id());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Receipt_Item save(Receipt_Item obj, int id) {
        String sql = "INSERT INTO receipt_item (PD_Name,RCIT_Amount,RCIT_Price,RCIT_Total,RC_ID,PD_ID)"
                + "VALUES (?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, obj.getPd_name());
            stmt.setInt(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());
            stmt.setDouble(4, obj.getTotal());
            stmt.setInt(5, id);
            stmt.setInt(6, obj.getPd_id());
            stmt.executeUpdate();
            id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Receipt_Item update(Receipt_Item obj, int id) {
        String sql = "UPDATE receipt_item"
                + " SET PD_Name = ? ,RCIT_Amount = ? ,RCIT_Price = ? ,RCIT_Total = ?,RC_ID = ?,PD_ID = ?"
                + " WHERE RCIT_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, obj.getPd_name());
            stmt.setInt(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());
            stmt.setDouble(4, obj.getTotal());
            stmt.setInt(5, obj.getRc_id());
            stmt.setInt(6, id);
            stmt.setInt(7, obj.getId());

            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Receipt_Item obj) {
        String sql = "DELETE FROM receipt_item WHERE RCIT_ID =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Receipt_Item> getAll(String where, String order) {
        ArrayList<Receipt_Item> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt_Item item = Receipt_Item.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt_Item update(Receipt_Item obj) {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        return null;
    }

}
