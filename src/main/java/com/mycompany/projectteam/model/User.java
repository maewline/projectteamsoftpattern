/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kiira
 */
public class User {

    private int id;
    private String Username;
    private String Password;
    private int Position;

    public User(int id, String Username, String Password, int Position) {
        this.id = id;
        this.Username = Username;
        this.Password = Password;
        this.Position = Position;
    }

    public User(String Username, String Password, int Position) {
        this.id = -1;
        this.Username = Username;
        this.Password = Password;
        this.Position = Position;
    }

    public User() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public int getPosition() {
        return Position;
    }

    public void setPosition(int Position) {
        this.Position = Position;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", Username=" + Username + ", Password=" + Password + ", Position=" + Position + '}';
    }

    public static User fromRS(ResultSet rs) {
        User user = new User();
        try {
            user.setId(rs.getInt("User_ID"));
            user.setUsername(rs.getString("User_Username"));
            user.setPassword(rs.getString("User_Password"));
            user.setPosition(rs.getInt("User_Position"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return user;
    }
}
