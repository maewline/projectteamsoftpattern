/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Member {

    private int id;
    private String firstname;
    private String lastname;
    private String phone;
    private int score;

    public Member(int id, String firstname, String lastname, String phone, int score) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.score = score;

    }

    public Member(String firstname, String lastname, String phone, int score) {
        this.id = -1;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.score = score;

    }

    public Member() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public static Member fromRS(ResultSet rs) {
        Member member = new Member();
        try {
            member.setId(rs.getInt("M_ID"));
            member.setFirstname(rs.getString("M_FirstName"));
            member.setLastname(rs.getString("M_LastName"));
            member.setPhone(rs.getString("M_Phone"));
            member.setScore(rs.getInt("M_Score"));

        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return member;
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", phone=" + phone + ", score=" + score + '}';
    }

}
