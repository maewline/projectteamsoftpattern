/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.model;

import com.mycompany.projectteam.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Salary {

    private int id;
    private Date date;
    private double salary;
    private Employee employee;

    public Salary(int id, Date date, double salary, Employee employee) {
        this.id = id;
        this.date = date;
        this.salary = salary;
        this.employee = employee;
    }

    public Salary(Date date, double salary, Employee employee) {
        this.id = -1;
        this.date = date;
        this.salary = salary;
        this.employee = employee;
    }

    public Salary() {
        this.id = -1;
         this.date = new Date();
         this.employee = new Employee();
         this.salary = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        EmployeeDao empD = new EmployeeDao();
        try {
            int empID = rs.getInt("EP_ID");
            Employee emp = empD.get(empID);
            salary.setId(rs.getInt("S_ID"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = rs.getString("S_Date");
            salary.setDate(sdf.parse(date));
            salary.setSalary(rs.getDouble("S_Salary"));
            salary.setEmployee(emp);
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salary;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", date=" + date + ", salary=" + salary + ", employee=" + employee + '}';
    }

  

}
