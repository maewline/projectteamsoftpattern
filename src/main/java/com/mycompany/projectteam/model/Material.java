/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tud08
 */
public class Material {
    private int id;
    private String name;
    private String unit;
    private int min;
    private int qoh;
    private double  price;

    public Material(int id, String name, String unit, double price, int min, int qoh) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.min = min;
        this.qoh = qoh;
        this.price = price;
    }

   
    
    public Material(String name, String unit, double price, int min, int qoh) {
        this.id = -1;
        this.name = name;
        this.unit = unit;
        this.min = min;
        this.qoh = qoh;
        this.price = price;
    }
    
    public Material() {
        this.id = -1;
        this.name = "-";
        this.unit = "-";
        this.min = 0;
        this.qoh = 0;
        this.price = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getQoh() {
        return qoh;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", unit=" + unit + ", min=" + min + ", qoh=" + qoh + ", price=" + price + '}';
    }

     
    
    
    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("MT_ID"));
            material.setName(rs.getString("MT_Name"));
            material.setUnit(rs.getString("MT_Unit"));
            material.setMin(rs.getInt("MT_Min"));
            material.setQoh(rs.getInt("MT_QOH"));
            material.setPrice(rs.getDouble("MT_Price"));

        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }
}