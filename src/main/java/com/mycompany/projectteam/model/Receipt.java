/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Receipt {
   private int id;
    private Date timestamp;
    private Double total;
    private int discount;
    private int qty;

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<Receipt_Item> getReceiptItem() {
        return receiptItem;
    }

    public void setReceiptItem(ArrayList<Receipt_Item> receiptItem) {
        this.receiptItem = receiptItem;
    }
    private Double cash;
    private int m_id;
    private int ep_id;
    private ArrayList<Receipt_Item> receiptItem = new ArrayList<>();

    public ArrayList<Receipt_Item> getReceipts() {
        return receiptItem;
    }

    public void setReceipts(ArrayList<Receipt_Item> receipts) {
        this.receiptItem = receipts;
    }

    public Receipt(int id, Date timestamp, Double total, int discount, Double cash, int m_id, int ep_id) {
        this.id = id;
        this.timestamp = new Date();
        this.total = total;
        this.discount = discount;
        this.cash = cash;
        this.m_id = m_id;
        this.ep_id = ep_id;
    }

    public Receipt(Date timestamp, Double total, int discount, Double cash, int m_id, int ep_id) {
        this.id = -1;
        this.timestamp = new Date();
        this.total = total;
        this.discount = discount;
        this.cash = cash;
        this.m_id = m_id;
        this.ep_id = ep_id;
    }

    public Receipt() {
        this.id = -1;
        this.timestamp = new Date();
        this.total = 0.0;
        this.discount = 0;
        this.cash = 0.0;
        this.m_id = 0;
        this.ep_id = 0;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    public int getM_id() {
        return m_id;
    }

    public void setM_id(int m_id) {
        this.m_id = m_id;
    }

    public int getEp_id() {
        return ep_id;
    }

    public void setEp_id(int ep_id) {
        this.ep_id = ep_id;
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("RC_ID"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String x = rs.getString("RC_Timestamp");
            receipt.setTimestamp(df.parse(x));
            receipt.setTotal(rs.getDouble("RC_Total"));
            receipt.setDiscount(rs.getInt("RC_Discount"));
            receipt.setCash(rs.getDouble("RC_Cash"));
            receipt.setEp_id(rs.getInt("EP_ID"));
            receipt.setM_id(rs.getInt("M_ID"));

        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return receipt;
    }

    public void addReciept(Product product, int qty) {
        Receipt_Item rcItem = new Receipt_Item(product.getName(), qty, product.getPrice(), (int) (product.getPrice() * qty), this.getId(), product.getId());
        receiptItem.add(rcItem);
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", timestamp=" + timestamp + ", total=" + total + ", discount=" + discount + ", cash=" + cash + ", m_id=" + m_id + ", ep_id=" + ep_id + '}';
    }

    public void remove(int index) {
        receiptItem.remove(index);
    }

    public int getSize() {
        return receiptItem.size();
    }

    public void showDetail() {
        for (Receipt_Item receipt_Item : receiptItem) {
            System.out.println(receipt_Item);
        }
    }

}
