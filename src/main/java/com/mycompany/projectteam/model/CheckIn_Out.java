/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.model;

import com.mycompany.projectteam.dao.EmployeeDao;
import com.mycompany.projectteam.dao.SalaryDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class CheckIn_Out {

   
    private int id;
    private Date time_start;
    private Date time_end;
    private int hour;
    private String payment_status;
    private Employee employee;
    private int salary;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public CheckIn_Out(int id, Date time_start, Date time_end, int hour, String payment_status, Employee employee, int salary) {
        this.id = id;
        this.time_start = time_start;
        this.time_end = time_end;
        this.hour = hour;
        this.payment_status = payment_status;
        this.employee = employee;
        this.salary = salary;
    }

    public CheckIn_Out(Date time_start, Date time_end, int hour, String payment_status, Employee employee, int salary) {
        this.id = -1;
        this.time_start = time_start;
        this.time_end = time_end;
        this.hour = hour;
        this.payment_status = payment_status;
        this.employee = employee;
        this.salary = salary;
    }

    public CheckIn_Out() {
        this.id = -1;
//        this.time_start = new Date();
//        this.time_end = new Date();
//        this.hour = 0;
//        this.payment_status = "N";
//        this.employee = employee;
        //this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime_start() {
        return time_start;
    }

    public void setTime_start(Date time_start) {
        this.time_start = time_start;
    }

    public Date getTime_end() {
        return time_end;
    }

    public void setTime_end(Date time_end) {
        this.time_end = time_end;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public static CheckIn_Out fromRS(ResultSet rs) {
        CheckIn_Out checkin_out = new CheckIn_Out();
        EmployeeDao employeeDao = new EmployeeDao();
        SalaryDao salaryDao = new SalaryDao();
        try {
            checkin_out.setEmployee(employeeDao.get(rs.getInt("EP_ID")));
            checkin_out.setSalary(rs.getInt("S_ID"));
            checkin_out.setId(rs.getInt("CIO_ID"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String in = rs.getString("CIO_Time_Start");
            String out = rs.getString("CIO_Time_End");
            checkin_out.setTime_start(sdf.parse(in));
            checkin_out.setTime_end(sdf.parse(out));
            checkin_out.setHour(rs.getInt("CIO_Hour"));
            checkin_out.setPayment_status(rs.getString("CIO_Payment_Status"));

        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(CheckIn_Out.class.getName()).log(Level.SEVERE, null, ex);
        }
        return checkin_out;
    }

    @Override
    public String toString() {
        return null;
    }
}
