/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.model;

import com.mycompany.projectteam.dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public Product(String name,double price, String image) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public Product() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("PD_id"));
            product.setName(rs.getString("PD_Name"));
            product.setPrice(rs.getDouble("PD_Price"));
            product.setImage(rs.getString("PD_Image"));

        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
        public static ArrayList<Product> getMockProductList() {
        ProductDao productDao = new ProductDao();
        ArrayList<Product> list = new ArrayList<>();
        for (Product product : productDao.getAll()) {
            list.add(product);
        }
        return list;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
}
