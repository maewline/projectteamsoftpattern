/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.model;

import com.mycompany.projectteam.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hanam
 */
public class CheckList {
private int id;
    private Date date;
    private Employee employee;
    private ArrayList<CheckListItem> checkListItem;

    public CheckList(int id, Date date, Employee employee) {
        this.id = id;
        this.date = date;
        this.employee = employee;
//        this.checkListItem = checkListItem;
    }

    public CheckList() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CheckList{" + "id=" + id + ", date=" + date + '}';
    }

    public static CheckList fromRS(ResultSet rs) {
        CheckList checkList = new CheckList();
        Employee emp = new Employee();
        EmployeeDao empD = new EmployeeDao();
        try {
            int empID = rs.getInt("EP_ID");
            emp = empD.get(empID);
            checkList.setId(rs.getInt("CL_ID"));
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//            String xxx = rs.getString("CL_Date");
//
//            Date date = sdf.parse(xxx);
            String date = rs.getString("CL_Date");
            checkList.setDate(sdf.parse(date));
            checkList.setEmployee(emp);
        } catch (SQLException ex) {
            Logger.getLogger(CheckList.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) { 
            Logger.getLogger(CheckList.class.getName()).log(Level.SEVERE, null, ex);
        }
        return checkList;
    }
}
