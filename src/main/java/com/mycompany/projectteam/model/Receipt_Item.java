/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class Receipt_Item {
 private int id;
    private String pd_name;
    private int amount;
    private double price;
    private int total;
    private int rc_id;
    private int pd_id;

    public Receipt_Item(int id, String pd_name, int amount, double price, int total, int rc_id, int pd_id) {
        this.id = id;
        this.pd_name = pd_name;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.rc_id = rc_id;
        this.pd_id = pd_id;
    }

    public Receipt_Item(String pd_name, int amount, double price, int total, int rc_id, int pd_id) {
        this.id = -1;
        this.pd_name = pd_name;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.rc_id = rc_id;
        this.pd_id = pd_id;
    }

    public Receipt_Item() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPd_name() {
        return pd_name;
    }

    public void setPd_name(String pd_name) {
        this.pd_name = pd_name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return price * amount;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getRc_id() {
        return rc_id;
    }

    public void setRc_id(int rc_id) {
        this.rc_id = rc_id;
    }

    public int getPd_id() {
        return pd_id;
    }

    public void setPd_id(int pd_id) {
        this.pd_id = pd_id;
    }

    public double getTotals() {
        return amount * price;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "id=" + id + ", pd_name=" + pd_name + ", amount=" + amount + ", price=" + price + ", total=" + total + '}';
    }

    public static Receipt_Item fromRS(ResultSet rs) {
        Receipt_Item receipt_item = new Receipt_Item();
        try {
            receipt_item.setId(rs.getInt("RCIT_ID"));
            receipt_item.setPd_name(rs.getString("PD_Name"));
            receipt_item.setAmount(rs.getInt("RCIT_Amount"));
            receipt_item.setPrice(rs.getDouble("RCIT_Price"));
            receipt_item.setTotal(rs.getInt("RCIT_Total"));
            receipt_item.setRc_id(rs.getInt("RC_ID"));
            receipt_item.setPd_id(rs.getInt("PD_ID"));

        } catch (SQLException ex) {
            Logger.getLogger(Receipt_Item.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt_item;
    }
}
