/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.model;

import com.mycompany.projectteam.dao.CheckListDao;
import com.mycompany.projectteam.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hanam
 */
public class CheckListItem {
   private int id;
    private int quantity;
    private String materialUnit;
    private String materialName;
    private int materialMin;
    private int materialQoh;
    private CheckList checklist;
    private Material material;

    public CheckListItem(int id, int quantity, String materialUnit, String materialName, int materialMin, int materialQoh, CheckList checklist, Material material) {
        this.id = id;
        this.quantity = quantity;
        this.materialUnit = materialUnit;
        this.materialName = materialName;
        this.materialMin = materialMin;
        this.materialQoh = materialQoh;
        this.checklist = checklist;
        this.material = material;
    }

    public CheckListItem(int quantity, String materialUnit, String materialName, int materialMin, int materialQoh, CheckList checklist, Material material) {
        this.id=-1;
        this.quantity = quantity;
        this.materialUnit = materialUnit;
        this.materialName = materialName;
        this.materialMin = materialMin;
        this.materialQoh = materialQoh;
        this.checklist = checklist;
        this.material = material;
    }

    
    public CheckListItem() {
        this.id = -1;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return materialUnit;
    }

    public void setUnit(String materialUnit) {
        this.materialUnit = materialUnit;
    }

    public String getName() {
        return materialName;
    }

    public void setName(String materialName) {
        this.materialName = materialName;
    }

    public int getMin() {
        return materialMin;
    }

    public void setMin(int materialMin) {
        this.materialMin = materialMin;
    }

    public int getQoh() {
        return materialQoh;
    }

    public void setQoh(int materialQoh) {
        this.materialQoh = materialQoh;
    }

    public CheckList getChecklist() {
        return checklist;
    }

    public void setChecklist(CheckList checklist) {
        this.checklist = checklist;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "CheckListItem{" + "id=" + id + ", quantity=" + quantity + ", materialUnit="
                + materialUnit + ", materialName=" + materialName + ", materialMin=" + materialMin 
                + ", materialQoh=" + materialQoh + ", checklist=" + checklist + ", material=" + material + '}';
    }

   
    
    
    public static CheckListItem fromRS(ResultSet rs) {
        CheckListItem checkListItem = new CheckListItem();
        CheckList checkList=new CheckList();
        CheckListDao checkDao = new CheckListDao();
        Material material = new Material();
        MaterialDao materialDao = new MaterialDao();
        try {
            checkList = checkDao.get(rs.getInt("CL_ID"));
            material = materialDao.get(rs.getInt("MT_ID"));
            checkListItem.setId(rs.getInt("CLI_ID"));
            checkListItem.setQuantity(rs.getInt("CLI_Quantity"));
            checkListItem.setUnit(rs.getString("MT_Unit"));
            checkListItem.setName(rs.getString("MT_Name"));
            checkListItem.setMin(rs.getInt("MT_Min"));
            checkListItem.setQoh(rs.getInt("MT_QOH"));
            checkListItem.setMaterial(material);
            checkListItem.setChecklist(checkList);
            
        } catch (SQLException ex) {
            Logger.getLogger(CheckList.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkListItem;
    }
}
