/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.service;



import com.mycompany.projectteam.dao.SaleDao;
import com.mycompany.projectteam.model.ReportSale;
import java.util.List;

/**
 *
 * @author Arthi
 */
public class ReportService {

    public List<ReportSale> getReportSaleByDay() {
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }

    public List<ReportSale> getReportSaleByMonth(int year) {
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }
    public List<ReportSale> getReportSaleByYear() {
        SaleDao dao = new SaleDao();
        return dao.getYearReport();
    }

}
