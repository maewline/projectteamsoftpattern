/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.service;

import com.mycompany.projectteam.dao.CheckIn_OutDao;
import com.mycompany.projectteam.model.CheckIn_Out;
import java.util.List;

/**
 *
 * @author tud08
 */
public class CheckInOutService {
    public List<CheckIn_Out> getCheckInOuts(){
        CheckIn_OutDao checkIn_OutDao = new CheckIn_OutDao();
        return checkIn_OutDao.getAll("CIO_Time_Start");
    }
    
        public List<CheckIn_Out> getStatus(int id){
        CheckIn_OutDao checkIn_OutDao = new CheckIn_OutDao();
        return checkIn_OutDao.getUnPaid(id);
    }
     
     public CheckIn_Out addNew(CheckIn_Out checkIn_Out){
         CheckIn_OutDao checkIn_OutDao = new CheckIn_OutDao();
         return checkIn_OutDao.save(checkIn_Out);
     }
}
