/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.service;

import com.mycompany.projectteam.dao.SalaryDao;
import com.mycompany.projectteam.model.Salary;
import java.util.List;

/**
 *
 * @author tud08
 */
public class SalaryService {
    public List<Salary> getSalarys(){
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAll("S_ID asc");
    }
    
    public Salary addNew(Salary salary){
         SalaryDao salaryDao = new SalaryDao();
         return salaryDao.save(salary);
     }
//    public List<Salary> getEmployees(){
//        SalaryDao salaryDao = new SalaryDao();
//        return salaryDao.getAllEmpl();
//    }
}
