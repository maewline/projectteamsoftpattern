/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.service;

import com.mycompany.projectteam.dao.MaterialDao;
import com.mycompany.projectteam.model.Material;
import java.util.List;

/**
 *
 * @author hanam
 */
public class MaterialService {
    public List<Material> getMaterials(){
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getAll("MT_Name asc");
    }
    
    public List<Material> getLackMaterials(){
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getAllLack();
    }

    public Material addNew(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.update(editedMaterial);
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }
}
