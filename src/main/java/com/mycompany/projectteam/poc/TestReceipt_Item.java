/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.poc;

import com.mycompany.projectteam.dao.Receipt_ItemDao;
import com.mycompany.projectteam.model.Receipt_Item;

/**
 *
 * @author admin
 */
public class TestReceipt_Item {

    public static void main(String[] args) {
        Receipt_Item receipt_item = new Receipt_Item("กาแฟร้อน", 2, 45.00, 80, 10, 15);
        Receipt_ItemDao receipt_itemDao = new Receipt_ItemDao();
//        receipt_itemDao.save(receipt_item);
//        System.out.println(receipt_item);
//        Receipt_Item get = receipt_itemDao.get(1);
//        System.out.println(get);
        //  receipt_itemDao.delete(receipt_itemDao.get(21));

        Receipt_Item update = receipt_itemDao.get(22);
        update.setPd_name("กาแฟ");

        receipt_itemDao.update(update);
    }
}
