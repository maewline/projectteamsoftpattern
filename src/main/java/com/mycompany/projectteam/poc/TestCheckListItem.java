/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.poc;

import com.mycompany.projectteam.dao.CheckListDao;
import com.mycompany.projectteam.dao.CheckListItemDao;
import com.mycompany.projectteam.dao.EmployeeDao;
import com.mycompany.projectteam.dao.MaterialDao;
import com.mycompany.projectteam.model.CheckList;
import com.mycompany.projectteam.model.CheckListItem;
import com.mycompany.projectteam.model.Employee;
import com.mycompany.projectteam.model.Material;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author hanam
 */
public class TestCheckListItem {
    public static void main(String[] args) throws ParseException {
        
        MaterialDao materialDao = new MaterialDao();
        Material material = materialDao.get(1);
        CheckListDao checkListDao = new CheckListDao();
        CheckList checkList = checkListDao.get(1);
//        
        CheckListItem checkListItem = new CheckListItem(13,20,"กิโล","Coffee",5,15,checkList,material);
        CheckListItemDao checkListItemDao = new CheckListItemDao();
//        checkListItemDao.save(checkListItem);
//        System.out.println(checkListItem);
        int quantity = 20;
        String unit = "กิโล";
        CheckListItem update = checkListItemDao.get(13);
        System.out.println(update);
        update.setQuantity(quantity);
        update.setUnit(unit);
//        System.out.println(update);
//        checkListItemDao.update(update);
//        checkListItemDao.delete(checkListItem);

    }
}
