/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.poc;

import com.mycompany.projectteam.dao.ProductDao;
import com.mycompany.projectteam.model.Product;

/**
 *
 * @author admin
 */
public class TestProductDao {

    public static void main(String[] args) {
        Product product = new Product("เค้กวนิลา", 40, "vanillaCake.jpg");
        ProductDao productDao = new ProductDao();
      //  productDao.save(product);
      //  productDao.delete(productDao.get(166));
      Product update = productDao.get(167);
      update.setName("เค้กนมสด");
      productDao.update(update);
    }
}
