/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.poc;

import com.mycompany.projectteam.dao.CheckListDao;
import com.mycompany.projectteam.dao.EmployeeDao;
import com.mycompany.projectteam.dao.MemberDao;
import com.mycompany.projectteam.model.CheckList;
import com.mycompany.projectteam.model.Employee;
import com.mycompany.projectteam.model.Member;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hanam
 */
public class TestCheckList {

    public static void main(String[] args) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String d = "12-10-2022 13:10:51";
        Date date = sdf.parse(d);
        EmployeeDao empDao = new EmployeeDao();
        Employee emp = empDao.get(1);
        CheckList checkList = new CheckList(4,date,emp);
        CheckListDao checkListDao = new CheckListDao();
//        System.out.println(checkList.getDate());
//        checkListDao.save(checkList);
//        
//        CheckList update = checkListDao.get(3);
//        String d2 = "12-10-2022 14:10:50";
//        System.out.println(update);
//        Date date2 = sdf.parse(d2);
//        update.setDate(date2);
//        checkListDao.update(update);
//        System.out.println(checkListDao.get(3));
        checkListDao.delete(checkList);

    }
}
