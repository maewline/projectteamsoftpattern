/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam.poc;

import com.mycompany.projectteam.dao.CheckIn_OutDao;
import com.mycompany.projectteam.dao.EmployeeDao;
import com.mycompany.projectteam.dao.SalaryDao;
import com.mycompany.projectteam.model.CheckIn_Out;
import com.mycompany.projectteam.model.Employee;
import com.mycompany.projectteam.model.Salary;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author hanam
 */
public class TestCheckInOut {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date in = sdf.parse("2022-10-13");
        Date out = sdf.parse("2022-10-13");
        Employee employee = new Employee();
        EmployeeDao employeeDao = new EmployeeDao();
        employee = employeeDao.get(1);
        Salary saraly = new Salary();
        SalaryDao salaryDao = new SalaryDao();
        saraly = salaryDao.get(2);
        
       // CheckIn_Out checkIn_Out = new CheckIn_Out(in, out, 0, "Y");
        CheckIn_OutDao checkIn_OutDao = new CheckIn_OutDao();
//        checkIn_OutDao.save(checkIn_Out);
//        
        CheckIn_Out  update = checkIn_OutDao.get(18);
//        String in2 = "2022-04-01";
//        Date date = sdf.parse(in2);
//        update.setTime_start(date);
//        checkIn_OutDao.update(update);
        checkIn_OutDao.delete(update);
    }
}
